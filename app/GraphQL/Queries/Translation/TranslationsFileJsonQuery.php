<?php

namespace App\GraphQL\Queries\Translation;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class TranslationsFileJsonQuery
{
    public function __invoke($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $file = resource_path().'/lang/'. $args['language_slug'] .'.json';

        if ( file_exists( $file ) ) {

            $file_lang_json = file_get_contents( $file );
            $response['jsonFile'] = json_decode( $file_lang_json, true );

        } else {


            $response['jsonFile'] = [];

        }

        $response['label']    = __( 'GL_SUCCESS' );
        $response['message']  = '';

        return $response;
    }
}

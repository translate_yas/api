<?php

namespace App\GraphQL\Queries\Translation;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use App\Models\Translation;


class TranslationSelectedQuery
{
    public function __invoke($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        // SELECT DISTINCT (tr_group) FROM `translations`
        $type     = Translation::distinct('type')->pluck('type')->toArray();
        $tr_group = Translation::distinct('tr_group')->pluck('tr_group')->toArray();

        $resType    = [];
        $resTrGroup = [];

        foreach( $type as $item ){

            $resType[]['label'] = $item;

        }

        foreach( $tr_group as $item ){

            $resTrGroup[]['label'] = $item;

        }

        $response['type']     = $resType;
        $response['tr_group'] = $resTrGroup;

        return $response;

    }
}

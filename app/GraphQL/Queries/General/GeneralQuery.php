<?php

namespace App\GraphQL\Queries\General;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class GeneralQuery
{
    protected function getDBinfo(){

        return DB::select( DB::raw('SHOW VARIABLES LIKE "version"') );

    }

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $getDBinfo = $this->getDBinfo();

        $response['system_info'] = [
            [
                'label' => 'Type',
                'info'  => env('APP_ENV')
            ],
            [
                'label' => 'Translate YAS',
                'info'  => '1.0.0'
            ],
            [
                'label' => 'Server date',
                'info'  => Carbon::now()->format('d F Y')
            ],
            [
                'label' => 'Server time',
                'info'  => Carbon::now()->format('H:i:s')
            ],
            [
                'label' => 'Timezone',
                'info'  => config('app.timezone')
            ],
            [
                'label' => 'Locale',
                'info'  => config('app.fallback_locale')
            ],
            [
                'label' => 'PHP version',
                'info'  => PHP_VERSION
            ],
            [
                'label' => 'Laravel version',
                'info'  => app()->version()
            ],
            [
                'label' => 'BD MySQL version',
                'info'  => $getDBinfo[0]->Value
            ]
        ];

        $response['site_api'] = [
            [
                'label' => 'APP url',
                'info'  => env('FRONT_URL')
            ],
            [
                'label' => 'APP api',
                'info'  => env('APP_URL') . '/graphql'
            ]
        ];

        return $response;
    }
}

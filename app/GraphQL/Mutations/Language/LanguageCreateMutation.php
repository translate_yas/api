<?php

namespace App\GraphQL\Mutations\Language;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\ErrorException;
use App\Helpers\ErrorValidatorHelper;
use App\Models\Language;


class LanguageCreateMutation
{
    protected $class_name = 'LanguageCreateMutation';

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $validator = Validator::make( $args, [
            'label'  => 'required|string|max:191',
            'slug'   => 'required|unique:languages|string|max:3',
            'status' => 'required|boolean',
        ] );

        if ( $validator->fails() ) {

            throw new ErrorException(
                $this->class_name,
                'resolve',
                __( 'CR_GL_ERROR' ),
                ErrorValidatorHelper::get_error_validator( $validator )
            );

        }

        $response['label']    = __('GL_SUCCESS');
        $response['message']  = __('GL_UPDATE_SUCCESS');
        $response['language'] = Language::create( $args );

        return $response;
    }
}

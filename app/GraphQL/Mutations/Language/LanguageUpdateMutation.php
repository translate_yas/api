<?php

namespace App\GraphQL\Mutations\Language;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\ErrorException;
use App\Helpers\ErrorValidatorHelper;
use App\Models\Language;


class LanguageUpdateMutation
{
    protected $class_name = 'LanguageUpdateMutation';

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        if( empty( $language = Language::where( 'id', $args['id'] ?? null )->first() ) ){

            throw new ErrorException(
                $this->class_name,
                'resolve',
                __('GL_ERROR'),
                __('GL_NotExist'),
            );

        }

        $validator = Validator::make( $args, [
            'label'  => 'string|max:191',
            'slug'   => !empty( $args['id'] ) && ( $args['slug'] == $language->slug ) ? 'string|max:3' : 'unique:languages|string|max:3',
            'status' => 'boolean',
        ] );

        if ( $validator->fails() ) {

            throw new ErrorException(
                $this->class_name,
                'resolve',
                __( 'CR_GL_ERROR' ),
                ErrorValidatorHelper::get_error_validator( $validator )
            );

        }

        $language->fill( $args );
        $language->update();

        $response['label']    = __('GL_SUCCESS');
        $response['message']  = __('GL_UPDATE_SUCCESS');
        $response['language'] = $language;

        return $response;
    }
}

<?php

namespace App\GraphQL\Mutations\Language;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use App\Exceptions\ErrorException;
use App\Models\Language;

class LanguageDeleteMutation
{
    protected $class_name = 'LanguageDeleteMutation';

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        if( empty ( $language = Language::whereId( $args['id'] ?? null )->first() ) ){

            throw new ErrorException(
                $this->class_name,
                'resolve',
                __('GL_ERROR'),
                __('GL_NotExist')
            );

        }

        $language->delete();

        $response['label']   = __('GL_SUCCESS');
        $response['message'] = __('GL_DELETE_SUCCESS');

        return $response;
    }
}

<?php

namespace App\GraphQL\Mutations\Translation;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use App\Models\Translation;


class TranslationDeleteMutation
{
    protected $class_name = 'TranslationDeleteMutation';

    public function resolve( $root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo )
    {
        Translation::destroy( $args['id'] );

        $response['label']    = __( 'GL_SUCCESS' );
        $response['message']  = __( 'GL_DELETE_SUCCESS');

        return $response;
    }
}

<?php

namespace App\GraphQL\Mutations\Translation;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use App\Models\Translation;


class TranslationUpdateMutation
{
    protected $class_name = 'TranslationUpdateMutation';

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $parrentTranslation = Translation::updateOrCreate(
            [
                'language_slug' => $args['child_slug'],
                'type'          => $args['type'],
                'tr_group'      => $args['tr_group'],
                'tr_key'        => $args['tr_key']
            ],
            ['value' => $args['value']]
        );

        $response['label']    = __( 'GL_SUCCESS' );
        $response['message']  = __( 'GL_UPDATE_SUCCESS');
        $response['translation'] = $parrentTranslation ?? null;

        return $response;
    }
}

<?php

namespace App\GraphQL\Mutations\Translation;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use App\Exceptions\ErrorException;
use Illuminate\Support\Facades\Storage;
use App\Models\Translation;
use App\Models\Language;


class TranslationImportMutation
{
    protected $class_name = 'TranslationImportMutation';

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $translations     = [];
        $codeTranslations = [];

        if ( empty( Language::where( 'slug', $args['language_slug'] )->first() ) ) {

            throw new ErrorException(
                $this->class_name,
                'findLanguage',
                __('GL_ERROR'),
                __('GL_NotExist')
            );

        }

        if ( ( array_search( $args['file']->getClientOriginalExtension(), ['json', 'php'] ) ) === false ) {

            throw new ErrorException(
                $this->class_name,
                'resolve',
                __('GL_FAILED'),
                __('Translation_FAILED_Import')
            );

        }

        $folder    = 'translations/import/';
        $name_file = $args['type'] . '-' . $args['file']->getClientOriginalName();
        $file_path = $folder . $name_file;
        $path_scan = storage_path() . '/app/public/translations/import';

        /* save file in server */
        Storage::disk('public')->putFileAs( $folder, $args['file'], $name_file );


        $separator = "_";

        if( $args['file']->getClientOriginalExtension() == 'php' ){

            /* parser php file */
            $codeTranslations = Translation::findTranslationsInCode( $path_scan );

            // $translations     = Translation::createNewTranslations( $codeTranslations, $args['language_slug'], $args['type'] );


        } elseif( $args['file']->getClientOriginalExtension() == 'json' ){

            /* parser json file */
            $file_json        = file_get_contents( $path_scan . '/' . $name_file );
            $codeTranslations = json_decode( $file_json, true );
            $separator        = ".";

        }

        $translations = Translation::createNewTranslations( $codeTranslations, $args['language_slug'], $args['type'], $separator );

        /* delete file from server */
        Storage::disk('public')->delete( $file_path );

        $response['label']        = __( 'GL_SUCCESS' );
        $response['message']      = __( 'Translation_SUCCESS_Import');
        $response['translations'] = $translations;

        return $response;
    }
}

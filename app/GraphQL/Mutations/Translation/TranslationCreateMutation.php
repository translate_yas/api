<?php

namespace App\GraphQL\Mutations\Translation;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\ErrorException;
use App\Helpers\ErrorValidatorHelper;
use App\Models\Translation;


class TranslationCreateMutation
{
    protected $class_name = 'TranslationCreateMutation';

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $findTranslation = Translation::where( 'type', $args['type'] )
            ->where( 'language_slug', $args['language_slug'] )
            ->where( 'tr_group', $args['tr_group'] )
            ->where( 'tr_key', $args['tr_key'] )
            ->first();

        if( $findTranslation ){
            throw new ErrorException(
                $this->class_name,
                'resolve',
                __( 'GL_FAILED' ),
                __( 'GL_RecordExist' ),
            );
        }

        $validator = Validator::make( $args, [
            'type'           => 'required|string|max:191',
            'language_slug'  => 'required|exists:languages,slug',
            'tr_group'       => 'required|string|max:191',
            'tr_key'         => 'required|string|max:191',
            'value'          => 'string|max:5000',
        ] );

        if ( $validator->fails() ) {

            throw new ErrorException(
                $this->class_name,
                'resolve',
                __( 'CR_GL_ERROR' ),
                ErrorValidatorHelper::get_error_validator( $validator )
            );

        }

        $translation = Translation::create( $args );

        $response['label']    = __( 'GL_SUCCESS' );
        $response['message']  = __( 'GL_CREATE_SUCCESS');
        $response['translation'] = $translation ?? null;

        return $response;
    }
}

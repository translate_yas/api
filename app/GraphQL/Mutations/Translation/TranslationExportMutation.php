<?php

namespace App\GraphQL\Mutations\Translation;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Illuminate\Support\Facades\Storage;
use App\Models\Translation;


class TranslationExportMutation
{
    protected $class_name = 'TranslationExportMutation';

    public function resolve( $root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo )
    {
        $translations = Translation::getSearchLike( $args );

        $file_name = 'export';

        if( isset( $args['type'] ) ){

            $translations->where( 'type', $args['type'] );
            $file_name = $args['type'];

        }

        if( isset( $args['tr_group'] ) ){

            $translations->where( 'tr_group', $args['tr_group'] );
            $file_name = $file_name . '_' . $args['tr_group'];

        }

        if( isset( $args['language_slug'] ) ){

            $translations->where( 'language_slug', $args['language_slug'] );
            $file_name = $file_name . '_' . $args['language_slug'];
        }

        $contents  = [];

        $folder    = 'translations/export/';
        $filename  = $file_name . '.json';
        $full_path = $folder . $filename;

        foreach( $translations->get()->toArray() as $value ){

            $contents[$value['tr_key']] = $value['value'];

        }

        ksort( $contents );

        Storage::disk('public')->put( $full_path, json_encode( $contents, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT ) );

        $zip_file     = $file_name . '.zip';
        $zip_path     = $folder . $zip_file;
        $storage_path = storage_path() . '/app/public/' . $full_path;

        $zip = new \ZipArchive();
        $zip->open( storage_path() . '/app/public/' . $zip_path, \ZipArchive::CREATE | \ZipArchive::OVERWRITE );
        $zip->addFile( $storage_path, $filename );
        $zip->close();

        $response['label']   = __('GL_SUCCESS');
        $response['message'] = __('Translation_SUCCESS_Export');
        $response['file']    = env('APP_URL') . Storage::url( $zip_path );

        return $response;
    }
}

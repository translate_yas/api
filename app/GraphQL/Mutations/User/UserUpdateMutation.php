<?php

namespace App\GraphQL\Mutations\User;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\ErrorException;
use App\Helpers\ErrorValidatorHelper;
use App\Models\User;


class UserUpdateMutation
{
    protected $class_name = 'UserUpdateMutation';

    public function resolve( $root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo )
    {
        $user = isset( $args['id'] ) ? User::whereId( $args['id'] )->first() : $context->user();

        if( empty ( $user ) ){

            throw new ErrorException(
                $this->class_name,
                'resolve',
                __('GL_ERROR'),
                __('User_NotExist')
            );

        }

        $validator = Validator::make( $args, [
            'name'     => 'string|max:191',
            'lang'     => 'exists:languages,slug',
            'role'     => 'in:guest,admin,developer',
            'status'   => 'in:new,active,block',
            'password' => 'string|min:8',
            'email'    => !empty( $args['email'] ) && $args['email'] != $user->email ? 'email|unique:users' : '',
        ]);

        if ( $validator->fails() ) {

            throw new ErrorException(
                $this->class_name,
                'userUpdate',
                __('User_UPDATE_FAILED_label'),
                ErrorValidatorHelper::get_error_validator( $validator )
            );

        }

        if( isset( $args['password'] ) && !empty( $args['password'] ) ){
            $args['password'] = bcrypt( $args['password'] );
        }

        $user->fill( $args );
        $user->update();

        $response['label']   = __('GL_SUCCESS');
        $response['message'] = __('GL_UPDATE_SUCCESS');
        $response['user']    = $user;

        return $response;
    }
}

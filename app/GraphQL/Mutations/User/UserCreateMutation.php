<?php

namespace App\GraphQL\Mutations\User;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\ErrorException;
use App\Helpers\ErrorValidatorHelper;
use App\Models\User;


class UserCreateMutation
{
    protected $class_name = 'UserCreateMutation';

    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $validator = Validator::make( $args, [
            'name'          => 'required|string|max:191',
            'email'         => 'required|email|unique:users',
            'lang'          => 'exists:languages,slug',
            'role'          => 'in:guest,admin,developer',
            'status'        => 'in:new,active,block',
            'password'      => 'required|string|min:8',
        ]);

        if ( $validator->fails() ) {

            throw new ErrorException(
                $this->class_name,
                'userCreate',
                __('GL_CREATE_FAILED'),
                ErrorValidatorHelper::get_error_validator( $validator )
            );

        }

        $user = User::create([
            'name'          => $args['name'],
            'email'         => $args['email'],
            'lang'          => $args['lang'] ?? User::LANGUAGE_EN,
            'role'          => $args['role'] ?? User::ROLE_GUEST,
            'status'        => $args['status'] ?? User::STATUS_ACTIVE,
            'password'      => bcrypt( $args['password'] ),
        ]);

        $response['label']    = __( 'GL_SUCCESS' );
        $response['message']  = __( 'GL_CREATE_SUCCESS');
        $response['user']     = $user;

        return $response;
    }
}

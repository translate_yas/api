<?php

namespace App\GraphQL\Mutations\User;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Joselfonseca\LighthouseGraphQLPassport\GraphQL\Mutations\BaseAuthResolver;
use Nuwave\Lighthouse\Exceptions\AuthenticationException;
use App\Exceptions\ErrorException;
use App\Models\User;

class UserLoginMutation extends BaseAuthResolver
{
    protected $class_name = 'UserLoginMutation';

    public function __invoke($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $user = User::where( 'email', $args['username'] ?? null )->first();

        if( empty( $user ) ){

            throw new ErrorException(
                $this->class_name,
                'login',
                __('Login_UserLoginMutation_FAILED_label'),
                __('User_NotExist'),
            );

        }

        if( $user->status == User::STATUS_BLOCK ){

            throw new ErrorException(
                $this->class_name,
                'login',
                __('GL_AccessDenied'),
                __('USER_BLOCKED')
            );

        }

        $args['username'] = $user->email;

        $credentials = $this->buildCredentials( $args );

        try{

            $response = $this->makeRequest( $credentials );

        } catch ( AuthenticationException $e ){

            throw new ErrorException(
                $this->class_name,
                'login',
                __('Login_UserLoginMutation_FAILED_label'),
                __('Login_UserLoginMutation_FAILED_message')
            );

        }

        $response['user'] = $user;

        return $response;

    }
}

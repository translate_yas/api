<?php

namespace App\GraphQL\Mutations\User;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Illuminate\Support\Facades\Auth;
use Joselfonseca\LighthouseGraphQLPassport\Events\UserLoggedOut;
use Joselfonseca\LighthouseGraphQLPassport\Exceptions\AuthenticationException;
use App\Models\User;

class UserLogoutMutation
{
    public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        if (! Auth::guard('api')->check()) {
            throw new AuthenticationException('Not Authenticated', 'Not Authenticated');
        }

        $user = Auth::guard('api')->user();

        // revoke user's token
        Auth::guard('api')->user()->token()->revoke();

        event( new UserLoggedOut( $user ) );

        return [
            'status'  => __('User_TOKEN_REVOKED'),
            'message' => __('User_SESSION_Closed'),
        ];
    }
}

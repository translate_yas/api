<?php

namespace App\GraphQL\Mutations\User;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use App\Exceptions\ErrorException;
use App\Models\User;


class UserDeleteMutation
{
    protected $class_name = 'UserDeleteMutation';

    public function resolve( $root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo )
    {
        if( $context->user()->id == $args['id'] ){

            throw new ErrorException(
                $this->class_name,
                'userDelete',
                __('GL_FAILED'),
                __('User_DELETE_ERROR_yourself')
            );

        }

        if( empty ( $user = User::whereId( $args['id'] ?? null )->first() ) ){

            throw new ErrorException(
                $this->class_name,
                'userDelete',
                __('GL_ERROR'),
                __('User_NotExist')
            );

        }

        $user->delete();

        $response['label']   = __('GL_SUCCESS');
        $response['message'] = __('GL_DELETE_SUCCESS');

        return $response;
    }
}

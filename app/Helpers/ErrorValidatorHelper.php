<?php

namespace App\Helpers;

class ErrorValidatorHelper
{
    public static function get_error_validator( $validator )
    {
        foreach( $validator->messages()->getMessages() as $field_name => $messages ){
            return $field_name . ': ' . $messages[0];
        }
    }
}

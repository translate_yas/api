<?php

namespace App\Helpers;

class TranslateHelper
{

    public function translateKeyString()
    {
        $global = [
            __('GL_SUCCESS', ['text' => 'Success']),
            __('GL_ERROR', ['text' => 'Error']),
            __('GL_FAILED', ['text' => 'Failed']),
            __('GL_NotExist', ['text' => 'Sorry, this does not exist']),
            __('GL_RecordExist', ['text' => 'Sorry, record exists with this data']),
            __('GL_AccessDenied', ['text' => 'Sorry, access denied']),
            __('GL_ACTION_FAILED_Rights', ['text' => 'Sorry, you dont have enough rights']),
            __('GL_CREATE_SUCCESS', ['text' => 'Record create successfully']),
            __('GL_CREATE_FAILED', ['text' => 'Record not created']),
            __('GL_UPDATE_SUCCESS', ['text' => 'Record updated successfully']),
            __('GL_UPDATE_FAILED', ['text' => 'Record not updated']),
            __('GL_DELETE_SUCCESS', ['text' => 'Record delete successfully']),
        ];

        $user = [
            __('Login_UserLoginMutation_FAILED_label', ['text' => 'Authentication failed']),
            __('Login_UserLoginMutation_FAILED_message', ['text' => 'The user credentials were incorrect']),

            __('User_EmailNotExist_message', ['text' => 'Sorry, this email does not exist']),

            __('User_BLOCKED', ['text' => 'Your account has been blocked by the administration. Please try to contact with administrator.']),

            __('User_NotExist', ['text' => 'Sorry, user does not exist']),
            __('User_SESSION_Closed', ['text' => 'Your session has been terminated']),
            __('User_TOKEN_REVOKED', ['text' => 'Token revoked']),
            __('User_DELETE_ERROR_yourself', ['text', 'You cannot delete yourself']),
            __('User_DELETE_ERROR_AdminLast', ['text', 'Sorry, you cannot delete the last one in the admin system']),
        ];

        $translation = [
            __('Translation_SUCCESS_Import', ['text' => 'Import successfully loaded into the database']),
            __('Translation_FAILED_Import', ['text' => 'Invalid file format, required format json or php']),
            __('Translation_SUCCESS_Export', ['text' => 'Export is generated, you can download']),
        ];

    }

}

<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use App\Exceptions\ErrorException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use App\Models\User;


class UserActionsMiddleware
{
    protected $class_name = 'UserActionsMiddleware';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /**
         * Set Language User & Set online User
         */
        $locale = config('app.fallback_locale');

        if ( Auth::user() ) {

            $user = Auth::user();

            if( $user->status == User::STATUS_BLOCK ){

                throw new ErrorException(
                    $this->class_name,
                    'userActions',
                    __('GL_AccessDenied'),
                    __('USER_BLOCKED')
                );

            }

            $locale = $user->lang;

            $last_visit_at = Carbon::parse( $user->last_visit_at );
            $diff_hours    = $last_visit_at->diffInHours( Carbon::now() );

            if( $user->last_visit_at === null || $diff_hours > 1 ){

                $user->last_visit_at = Carbon::now();
                $user->update();

            }

            Cache::put( 'user-is-online-' . $user->id, true, Carbon::now()->addMinutes(5) );
            Cache::put( 'user-last-active-' . $user->id, Carbon::now() );

        }

        App::setLocale( $locale );

        return $next($request);
    }
}

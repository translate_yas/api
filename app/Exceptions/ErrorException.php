<?php

namespace App\Exceptions;

use Exception;
use Nuwave\Lighthouse\Exceptions\RendersErrorsExtensions;

class ErrorException extends Exception implements RendersErrorsExtensions
{
    private $class;

    private $reason;

    private $action;

    public function __construct(string $class, string $action, string $message, string $reason)
    {
        parent::__construct($message);

        $this->class = $class;

        $this->action = $action;

        $this->reason = $reason;
    }

    public function isClientSafe(): bool
    {
        return true;
    }

    public function getCategory(): string
    {
        return $this->class;
    }

    public function extensionsContent(): array
    {
        return [
            'action' => $this->action,
            'reason' => $this->reason,
        ];
    }
}


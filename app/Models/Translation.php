<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Filesystem\Filesystem;
use Carbon\Carbon;


class Translation extends Model
{
    use HasFactory;

    protected $table = 'translations';

    protected $fillable = [
        'language_slug',
        'type',
        'tr_group',
        'tr_key',
        'value'
    ];

    public const TYPE_CORE = 'core';

    /**
     * ********** ********** START Scope Query #scope ********** ***********
     */

    public function scopeGetSearchLike( $query, array $args )
    {
        if( !isset( $args['text'] ) || empty( $args['text'] ) ){
            return $query->orderBy( 'created_at', 'desc' );
        }

        $search = str_replace( " ", "%", $args['text'] );

        return $query->where( function( $query ) use ( $search ){
            $query->where( 'value', 'like', '%'. $search .'%' )
                ->orWhere( 'tr_key', 'like', '%'. $search .'%' )
                ->orWhere( 'tr_group', 'like', '%'. $search .'%' );
            })->orderBy( 'created_at', 'desc' );
    }

    /**
     * ********** ********** END Scope Query ********** ***********
     */


    /**
     * ********** ********** Public Method #public_method ********** **********
     */

    public function getChildTranslation( string $child_slug )
    {
        $child_translate = Translation::where( 'language_slug', $child_slug )
            ->where( 'type', $this->type )
            ->where( 'tr_group', $this->tr_group )
            ->where( 'tr_key', $this->tr_key )
            ->first();

        return $child_translate ?? null;
    }

    public function getStatus(): string
    {
        if( strtotime( Carbon::now() ) == strtotime( $this->created_at ) ){

            return 'new';

        }

        if( strtotime( Carbon::now() ) != strtotime( $this->created_at ) && strtotime( Carbon::now() ) == strtotime( $this->updated_at ) ){

            return 'update';

        }

        return 'old';
    }

    /**
     * ********** ********** END Public Method ********** **********
     */


    /**
     * ********** ********** Static Method #static_method ********** **********
     */

    public static function findTranslationsInCode( $path_scan = false )
    {
        // $results = ['single' => [], 'group' => []];
        // $translationMethods = ['__'];

        $results = [];
        $disk    = new Filesystem();

        $path_scan ?? app_path();

        // This has been derived from a combination of the following:
        // * Laravel Language Manager GUI from Mohamed Said (https://github.com/themsaid/laravel-langman-gui)
        // * Laravel 5 Translation Manager from Barry vd. Heuvel (https://github.com/barryvdh/laravel-translation-manager)
        // $matchingPattern =
        //     '[^\w]'. // Must not start with any alphanum or _
        //     '(?<!->)'. // Must not start with ->
        //     '('.implode( '|', $translationMethods ) . ')'. // Must start with one of the functions
        //     "\(". // Match opening parentheses
        //     "[\'\"]". // Match " or '
        //     '('. // Start a new group to match:
        //     '.+'. // Must start with group
        //     ')'. // Close group
        //     "[\'\"]". // Closing quote
        //     "[\),]";  // Close parentheses or new parameter

        // [^\w](?<!->)(__)\([\'\"](.+)[\'\"][\),].+[\'\"](.+)[\'\"].+[\'\"](.+)[\'\"]\]\)
        // [^\w](?<!->)(__)\([\'\"](.+)[\'\"][\),]

        $matchingPattern = "[^\w](?<!->)__\(.*\),*";

        foreach ( $disk->allFiles( $path_scan ) as $file ) {

            if ( preg_match_all( "/$matchingPattern/siU", $file->getContents(), $matches ) ) {

                foreach ( $matches as $items ) {

                    foreach( $items as $item ){

                        if ( preg_match( "/__\(\s*[\'|\"](.+)[\'|\"]\s*\)/iU", $item, $arrayMatches ) ) {

                            if ( array_key_exists( $arrayMatches[1], $results ) ) continue;

                            $results[$arrayMatches[1]] = '';

                        }

                        if ( preg_match( "/__\(\s*[\'|\"](.+)[\'|\"].*[\[][\'|\"](.+)[\'|\"].+[\'|\"](.+)[\'|\"][\]]\s*\)/iU", $item, $arrayMatches ) ) {

                            $results[$arrayMatches[1]] = $arrayMatches[3];

                        }

                    }

                }

            }

        }

        return $results;

    }

    public static function createNewTranslations( $codeTranslations, $lang, $type, $separator = "_" )
    {
        ksort( $codeTranslations );

        $lang ?? config('app.fallback_locale');
        $type ?? Translation::TYPE_CORE;

        $models = [];

        foreach( $codeTranslations as $key => $value ){

            $tr_group = explode( $separator, $key );

            // use firstOrCreate or updateOrCreate
            $models[] = Translation::firstOrCreate(
                [
                    'tr_key'        => $key,
                    'type'          => $type,
                    'language_slug' => $lang,
                    'tr_group'      => $tr_group[0],
                ],
                [
                    'value' => $value
                ]
            );

        }

        return $models;
    }

    /**
     * ********** ********** END Static Method ********** **********
     */
}

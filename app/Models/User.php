<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;
use Laravel\Passport\HasApiTokens;


class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'lang',
        'role',
        'status',
        'last_visit_at',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public const LANGUAGE_EN = 'en';

    public const STATUS_NEW    = 'new';
    public const STATUS_ACTIVE = 'active';
    public const STATUS_BLOCK  = 'block';

    public const ROLE_DEVELOPER = 'developer';
    public const ROLE_ADMIN     = 'admin';
    public const ROLE_GUEST     = 'guest';


    /**
     * ********** ********** START Scope Query #scope ********** ***********
     */

    public function scopeGetSearchLike( $query, array $args )
    {
        if( !isset( $args['text'] ) || empty( $args['text'] ) ){
            return $query;
        }

        $search = $args['text'] ?? null;
        $search = str_replace( " ", "%", $search );

        return $query->where( function( $query ) use ( $search ){
                $query->where( 'name', 'like', '%'. $search .'%' )
                ->orWhere( 'email', 'like', '%'. $search .'%' );
            });
    }

    /**
     * ********** ********** END Scope Query ********** ***********
     */


    /**
     * ********** ********** Public Method #public_method ********** **********
     */

    public function isOnline()
    {
        return Cache::has( 'user-is-online-' . $this->id );
    }

    public function userLastActiveDate()
    {
        if ( Cache::has( 'user-last-active-' . $this->id ) ){
            return \Carbon\Carbon::parse( Cache::get( 'user-last-active-' . $this->id ) )->diffForHumans();
        }

        return false;
    }

    /**
     * ********** ********** END Public Method ********** **********
     */

}

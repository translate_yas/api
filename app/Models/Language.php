<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use HasFactory;

    protected $table = 'languages';

    protected $fillable = ['label', 'slug', 'status'];

    protected $casts = [
        'sync_date' => 'array'
    ];

    public $timestamps = false;
}

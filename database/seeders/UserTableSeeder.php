<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            array(
                'name'          => 'Aleksandr Yakovenko',
                'email'         => 'paffen.web@gmail.com',
                'email_verified_at' => null,
                'password'      => '$2y$10$gvIZrZQWBRG.hj2nvNZqyOKZ1R28mAm5xUJIA7aa5EuIqg4X1TgIC',
                'status'        => 'active',
                'role'          => 'developer',
                'lang'          => 'en',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            )
        ));
    }
}

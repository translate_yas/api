<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert(array(
            array(
                'label' => 'English', 'slug' => 'en', 'status' => true, 'sync_date' => null
            ),
			array(
                'label' => 'Deutsch', 'slug' => 'de', 'status' => true, 'sync_date' => null
            )
        ));
    }
}
